ABQ Tool
========

A tool to work with Abaqus files.  Much is planned; little is done.

Don't use it yet :)

So far a means of scanning an input deck very efficiently is available
at `gitlab.com/tbhartman/abqt/inp::InputFileBase.QuickParse`.

 * made for large files; target at least 10e6 lines per second
 * reports invalid comments (they must start in first column)
 * reports helpful error for files using CR or mixed line endings
   - i will not support them in the parser
   - but i will try to help...the `abqt normalize` tool corrects line endings

goals
-----

There are two primary targets for this project:

 * Print out a summary of an input deck
 * Given the name of an element set, remove that set from the deck

I had made a very handy tool in Python to do that several years ago.
I do not have access to that code.  This project is an attempt to re-write
that in go.
