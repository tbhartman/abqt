package main_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	abqt "gitlab.com/tbhartman/abqt"
)

func TestFilePositionAdd(t *testing.T) {
	p := abqt.NewFilePosition()
	p.Add(10)
	assert.EqualValues(t, abqt.FilePosition{10, 1, 11}, p)

	p.AddLine(10)
	assert.EqualValues(t, abqt.FilePosition{20, 2, 1}, p)

	p.AddPosition(abqt.FilePosition{5, 1, 6})
	assert.EqualValues(t, abqt.FilePosition{25, 2, 6}, p)

	p.AddPosition(abqt.FilePosition{5, 2, 6})
	assert.EqualValues(t, abqt.FilePosition{30, 3, 6}, p)
}
