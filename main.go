package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"sort"
	"time"

	"github.com/dustin/go-humanize"
	"gitlab.com/tbhartman/abqt/inp"
	"gitlab.com/tbhartman/alleolscanner"
)

var start time.Time

func init() {
	start = time.Now()
}

type sortCount struct {
	names []string
	count map[string]int
}

func (a sortCount) Len() int           { return len(a.names) }
func (a sortCount) Swap(i, j int)      { a.names[i], a.names[j] = a.names[j], a.names[i] }
func (a sortCount) Less(i, j int) bool { return a.count[a.names[i]] < a.count[a.names[j]] }

//
func summarize(r inp.ReadSeekNamer) error {
	start := time.Now()
	input := inp.NewInputFromNamedReader(r)
	parsedInput, err := input.QuickParse()
	duration := time.Now().Sub(start)
	if err != nil {
		fmt.Println("error reading input file:")
		if e, ok := err.(inp.ErrSyntax); ok {
			fmt.Printf(
				"  %s\n  line %d, column %d\n",
				e.Error(),
				e.Position().Line,
				e.Position().Column,
			)
		} else {
			fmt.Printf("  %s\n", err.Error())
		}
		return err
	}
	fmt.Println("summary")
	fmt.Printf("  size      : %s\n", humanize.Bytes(parsedInput.Size()))
	fmt.Printf("  lines     : %s\n", humanize.Comma(parsedInput.LineCount()))
	fmt.Printf("  comments  : %s\n", humanize.Comma(parsedInput.CommentCount()))
	fmt.Printf("  data lines: %s\n", humanize.Comma(parsedInput.DataLineCount()))
	fmt.Printf("  keywords  : %s\n", humanize.Comma(int64(len(parsedInput.Keywords()))))

	fmt.Println("top keywords")

	kwCount := make(map[string]int)
	kwNames := make([]string, 0)
	for _, kw := range parsedInput.Keywords() {
		if _, ok := kwCount[kw.Name()]; !ok {
			kwCount[kw.Name()] = 1
			kwNames = append(kwNames, kw.Name())
		} else {
			kwCount[kw.Name()]++
		}
	}
	sortable := sortCount{
		names: kwNames,
		count: kwCount,
	}
	sort.Sort(sortable)
	for i := len(sortable.names) - 1; i >= 0 && i >= len(sortable.names)-10; i-- {
		fmt.Printf("  *%s (x%d)\n", sortable.names[i], kwCount[sortable.names[i]])
	}
	fmt.Printf("parsed in : %s\n", duration.Truncate(time.Millisecond))
	parseRate := float64(parsedInput.LineCount()) / float64(duration.Microseconds())
	fmt.Printf("parse rate: %sM lines/s\n", humanize.CommafWithDigits(parseRate, 1))

	return nil
}

func run(args []string) int {
	var info string = `abqt usage:
    abqt help             show this help message
    abqt summarize INPUT  summarize input file INPUT
    abqt normalize INPUT  normalize line endings in INPUT
`
	var invalid bool
	var help bool
	var summarizeFlag bool
	var summarizeFile string
	var normalizeFlag bool
	var normalizeFile string
	switch {
	case len(args) <= 1:
		invalid = true
	case args[1] == "help":
		help = true
	case args[1] == "summarize" && len(args) == 3:
		summarizeFlag = true
		summarizeFile = args[2]
	case args[1] == "normalize" && len(args) == 3:
		normalizeFlag = true
		normalizeFile = args[2]
	default:
		invalid = true
	}
	if invalid || help {
		if invalid {
			fmt.Println("invalid arguments")
		}
		fmt.Print(info)
		if invalid {
			return 1
		}
		return 0
	}
	if normalizeFlag {
		return normalize(normalizeFile)
	}

	_ = summarizeFlag
	f, err := os.Open(summarizeFile)
	if err != nil {
		log.Println(err)
		return 2
	}
	defer f.Close()
	err = summarize(f)
	if err != nil {
		log.Println(err)
		return 2
	}
	return 0
}

func normalize(s string) int {
	g, err := os.Open(s)
	if err != nil {
		log.Println(err)
		return 2
	}
	defer g.Close()

	f, err := ioutil.TempFile("", "normalize_")
	if err != nil {
		log.Println(err)
		return 2
	}
	defer os.Remove(f.Name())
	defer f.Close()

	scanner := alleolscanner.NewScanner(g)
	writer := bufio.NewWriter(f)
	var lineEnding string = "\n"
	if runtime.GOOS == "windows" {
		lineEnding = "\r\n"
	}
	for scanner.Scan() {
		writer.WriteString(scanner.Text() + lineEnding)
	}
	err = writer.Flush()
	if err != nil {
		log.Println(err)
		return 2
	}
	f.Close()
	g.Close()
	err = os.Rename(f.Name(), s)
	if err != nil {
		log.Println(err)
		return 2
	}
	return 1
}

func main() {
	ret := run(os.Args)
	fmt.Println(time.Now().Sub(start).String())
	os.Exit(ret)
}
