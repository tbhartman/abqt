package inp

import (
	"fmt"
	"math"
	"strconv"
	"strings"
)

type state int

const (
	start state = iota
	star
	keywordName
	parameterName
	startParameterName
	value
	valueSingleQuote
	valueDoubleQuote
	valueEnd
	eol
)

func finalizeParam(kw *keyword, value string, noValue bool, quoted bool, addEmpty bool) {
	float, errFloat := strconv.ParseFloat(value, 64)
	integer, errInt := strconv.ParseInt(value, 10, 32)
	if quoted {
		kw.params[len(kw.params)-1].value = &parameterString{
			value:  value,
			quoted: true,
		}
	} else if noValue {
		kw.params[len(kw.params)-1].value = nil
	} else if errInt == nil {
		if errFloat == nil && float64(integer) == float {
			kw.params[len(kw.params)-1].value = &parameterInt{
				value: int(integer),
			}
		} else {
			kw.params[len(kw.params)-1].value = &parameterInt{
				value: int(integer),
			}
		}
	} else if errFloat == nil {
		if !strings.Contains(value, ".") && math.Floor(float) == float {
			kw.params[len(kw.params)-1].value = &parameterInt{
				value: int(float),
			}
		} else {
			kw.params[len(kw.params)-1].value = &parameterFloat{
				value: float,
			}
		}
	} else {
		// not quoted and not a number
		value = strings.TrimSpace(value)
		kw.params[len(kw.params)-1].value = &parameterString{
			value:  value,
			quoted: false,
		}
	}
	if addEmpty {
		kw.params = append(kw.params, &parameterBase{})
	}
}

func parseKeywordLine(b []byte) (*keyword, ErrSyntax) {
	// there may be a comment line in here, but at this point it must start "\n**"
	ret := &keyword{}
	status := start
	pos := FilePosition{
		Column: 1,
		Line:   1,
	}
	var err bool
	var paramValue string
FOR:
	for _, char := range b {
		switch status {
		case start:
			if char != '*' {
				err = true
				break
			}
			status = star
		case star:
			if char == '*' {
				err = true
				break FOR
			}
			status = keywordName
			fallthrough
		case keywordName:
			switch char {
			case ',':
				status = startParameterName
				ret.params = append(ret.params, &parameterBase{})
			case '\r':
				break
			case '\n':
				status = eol
			default:
				ret.name += string(char)
			}
		case startParameterName:
			if strings.IndexByte(" \t\n\r", char) >= 0 {
				break
			}
			paramValue = ""
			status = parameterName
			fallthrough
		case parameterName:
			switch char {
			case ',':
				status = startParameterName
				finalizeParam(ret, paramValue, true, false, true)
			case '=':
				paramValue = ""
				status = value
			case '\r':
				break
			case '\n':
				finalizeParam(ret, paramValue, false, false, false)
				status = eol
			default:
				ret.params[len(ret.params)-1].name += string(char)
			}
		case value:
			switch char {
			case ' ':
				fallthrough
			case '\t':
				break
			case '"':
				if len(paramValue) != 0 {
					err = true
					break FOR
				}
				status = valueDoubleQuote
			case '\'':
				if len(paramValue) != 0 {
					err = true
					break FOR
				}
				status = valueSingleQuote
			case ',':
				status = startParameterName
				finalizeParam(ret, paramValue, false, false, true)
			case '\r':
				break
			case '\n':
				finalizeParam(ret, paramValue, false, false, false)
				status = eol
			default:
				paramValue += string(char)
			}
		case valueDoubleQuote:
			switch char {
			case '"':
				status = valueEnd
			default:
				paramValue += string(char)
			}
		case valueSingleQuote:
			switch char {
			case '\'':
				status = valueEnd
			default:
				paramValue += string(char)
			}
		case valueEnd:
			switch char {
			case ' ':
				fallthrough
			case '\t':
				break
			case ',':
				status = startParameterName
				finalizeParam(ret, paramValue, false, true, true)
			case '\r':
				break
			case '\n':
				finalizeParam(ret, paramValue, false, true, false)
				status = eol
			default:
				err = true
				break FOR
			}
		case eol:
			switch char {
			case ' ':
				fallthrough
			case '\t':
				break
			default:
				err = true
				break FOR
			}
		default:
			panic(fmt.Sprintf("state is not expected: %v", status))
		}
		if char == '\n' {
			pos.Line++
			pos.Column = 1
		}
		pos.Position++
	}

	switch status {
	case start:
		err = true
	case star:
		err = true
	case startParameterName:
		err = true
	case valueSingleQuote:
		err = true
	case valueDoubleQuote:
		err = true
	case valueEnd:
		finalizeParam(ret, paramValue, false, true, false)
	case value:
		finalizeParam(ret, paramValue, false, false, false)
	case parameterName:
		finalizeParam(ret, paramValue, true, false, false)
	}

	if err {
		return nil, &errSyntax{
			err: "Syntax error",
			pos: pos,
		}
	}
	// clean up names
	ret.name = strings.ToUpper(strings.ReplaceAll(ret.name, " ", ""))
	for i, p := range ret.params {
		ret.params[i].name = strings.ToLower(strings.ReplaceAll(p.name, " ", ""))
	}
	return ret, nil

}
