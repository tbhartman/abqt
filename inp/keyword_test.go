package inp_test

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tbhartman/abqt/inp"
)

func getInput(t *testing.T, data string) inp.InputFileBase {
	return inp.NewInputFromNamedReader(
		inp.NewNamedReader(
			t.Name(),
			strings.NewReader(data),
		),
	)
}

func TestRoughParse(t *testing.T) {
	t.Skip()
	r := getInput(t, "*a\n*b\n*c\n")
	result, err := r.QuickParse()
	assert.NoError(t, err)
	if assert.NotNil(t, result) {
		assert.EqualValues(t, 3, result.LineCount())
		assert.EqualValues(t, 3, len(result.Keywords()))
		assert.EqualValues(t, 0, result.CommentCount())
	}
}

func TestIndentedComment(t *testing.T) {
	r := getInput(t, "*a\n**b\n **c\n*d\n")
	result, err := r.QuickParse()
	assert.Error(t, err)
	se, ok := err.(inp.ErrSyntax)
	if assert.True(t, ok) {
		assert.EqualValues(
			t,
			inp.FilePosition{
				Column:   2,
				Line:     3,
				Position: 8,
			},
			se.Position(),
		)
	}
	assert.Nil(t, result)
}

func getKeywordLine(t *testing.T, line string) inp.Keyword {
	input := getInput(t, line)
	result, err := input.QuickParse()
	assert.NoError(t, err)
	if assert.NotNil(t, result) {
		if assert.Greater(t, len(result.Keywords()), 0) {
			return result.Keywords()[0]
		}
	}
	t.FailNow()
	return nil
}

func TestKeyword(t *testing.T) {
	kwl := getKeywordLine(t, `*Heading`)
	assert.Equal(t, "HEADING", kwl.Name())

	kwl = getKeywordLine(t, `*Heading, pAram`)
	if assert.Equal(t, 1, len(kwl.Parameters())) {
		assert.Equal(t, "param", kwl.Parameters()[0].Name())
		assert.Nil(t, kwl.Parameters()[0].Value())
	}

	kwl = getKeywordLine(t, `*Heading, pAram=1`)
	if assert.Equal(t, 1, len(kwl.Parameters())) {
		value, ok := kwl.Parameters()[0].Value().(int)
		if assert.True(t, ok) {
			assert.Equal(t, 1, value)
		}
	}

	kwl = getKeywordLine(t, `*Heading, pAram=1e6`)
	if assert.Equal(t, 1, len(kwl.Parameters())) {
		value, ok := kwl.Parameters()[0].Value().(int)
		if assert.True(t, ok) {
			assert.EqualValues(t, 1e6, value)
		}
	}

	kwl = getKeywordLine(t, `*Heading, pAram=1.2e6`)
	if assert.Equal(t, 1, len(kwl.Parameters())) {
		value, ok := kwl.Parameters()[0].Value().(float64)
		if assert.True(t, ok) {
			assert.EqualValues(t, 1.2e6, value)
		}
	}

	kwl = getKeywordLine(t, `*Heading, pAram= hi`)
	if assert.Equal(t, 1, len(kwl.Parameters())) {
		value, ok := kwl.Parameters()[0].Value().(string)
		if assert.True(t, ok) {
			assert.Equal(t, "hi", value)
		}
	}

	kwl = getKeywordLine(t, `*Heading, pAram=" hi"`)
	if assert.Equal(t, 1, len(kwl.Parameters())) {
		value, ok := kwl.Parameters()[0].Value().(string)
		if assert.True(t, ok) {
			assert.Equal(t, " hi", value)
		}
	}
}

func TestKeywordLineFormat(t *testing.T) {
	for i, data := range [][2]string{
		{"*HEADING\n", "*Heading"},
		{"*HEADING, params=1\n", "*Heading,params=1"},
		{"*HEADING, params=\"1\"\n", "*Heading,params=\"1\""},
		{"*HEADING, params\n", "*Heading,\n params"},
	} {
		t.Run(fmt.Sprintf("%d", i), func(t *testing.T) {
			kwl := getKeywordLine(t, data[1])
			assert.Equal(t, data[0], kwl.String())
		})
	}
}

func TestInvalidComment(t *testing.T) {
	input := getInput(t, `*Heading
 ** invalid comment
`)
	_, err := input.QuickParse()
	assert.Error(t, err)
}

func TestMy(t *testing.T) {
	a := []byte("hello world")
	a[0] = 'b'
	fmt.Println(a)
	fmt.Println(b)
}
