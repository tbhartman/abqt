// Package keyword
package inp

import (
	"bufio"
	"fmt"
	"io"
	"strings"
	"sync"
	"unicode"

	"gitlab.com/tbhartman/alleolscanner"
)

const maxLineLen int = 100
const keywordLineIndent string = "     "

// maxKeywordLineContinuation is the maximum number of keyword continuation
// read before giving up with a syntax error.  This should be high enough so as
// to be unreasonable, but theoretically possible
const maxKeywordLineContinuation int = 1000

// ParseLevel describes how much of the input deck has been parsed.
type ParseLevel int

// How much to parse
const (
	ParseNone ParseLevel = iota
	ParseMinimal
	ParseKeywordPositions
	ParseParameters
	ParseDataSize
	ParseMesh
	ParseFull
)

type Namer interface {
	Name() string
}

type InputFileBase interface {
	io.Closer
	Namer
	Size() uint64
	QuickParse() (InputFileQuickParsed, error)
	FullParse() (InputFileFullParsed, error)
}

type InputFileQuickParsed interface {
	InputFileBase
	LineCount() int64
	DataLineCount() int64
	CommentCount() int64
	Keywords() []Keyword
	// FormatTo(w io.Writer) error
}

type InputFileFullParsed interface {
	InputFileQuickParsed
}

type Keyword interface {
	fmt.Stringer
	Namer
	Parameters() []Parameter
}

type Parameter interface {
	fmt.Stringer
	Namer
	Value() interface{}
}

type inputFile struct {
	io.Closer
	Namer
	r             io.ReadSeeker
	size          uint64
	lineCount     int64
	commentCount  int64
	dataLineCount int64
	quickParsed   bool
	fullParsed    bool
	keywords      []keyword
}

func (i *inputFile) Name() string {
	return i.Namer.Name()
}
func (i *inputFile) LineCount() int64 {
	return i.lineCount
}
func (i *inputFile) DataLineCount() int64 {
	return i.dataLineCount
}
func (i *inputFile) CommentCount() int64 {
	return i.commentCount
}
func (i *inputFile) Size() uint64 {
	return i.size
}
func (i *inputFile) Close() error {
	if i.Closer == nil {
		return nil
	}
	return i.Closer.Close()
}
func (i *inputFile) QuickParse() (InputFileQuickParsed, error) {
	if i.quickParsed {
		return i, nil
	}
	i.r.Seek(0, io.SeekStart)
	results := roughParse(i.r)
	if results.Error != nil {
		return nil, results.Error
	}

	i.lineCount = results.LineCount
	i.commentCount = results.CommentCount
	i.keywords = results.Keywords

	return i, results.Error
}
func (i *inputFile) FullParse() (InputFileFullParsed, error) {
	if i.fullParsed {
		return i, nil
	}
	panic("not yet implemented")
}

func (i *inputFile) Keywords() []Keyword {
	ret := make([]Keyword, len(i.keywords))
	for n, k := range i.keywords {
		ret[n] = &keyword{
			position: k.position,
			name:     k.name,
			params:   k.params,
		}
	}
	return ret
}

type keywordPositionData struct {
	start         FilePosition
	keywordLength int64
	dataLength    int64
	dataLines     int64
}

type keyword struct {
	position keywordPositionData
	name     string
	params   []*parameterBase
}

// String presents a normalized keyword line(s), with ending newline
func (k *keyword) String() string {
	var ret string
	var currLine string
	// start with keyword name
	currLine = "*" + k.name

	// add in each parameter
	var lengthAfterThisParam int
	for i, p := range k.params {
		// do not let ret get over maxLineLen chars
		pStr := p.String()
		// this will add a comma and a space to the line
		lengthAfterThisParam = len(ret) + 2 + len(pStr)
		if i < len(k.params)-1 {
			// there will be a comma after this too
			lengthAfterThisParam++
		}

		currLine += ","
		if lengthAfterThisParam > maxLineLen {
			// put in a newline
			currLine += "\n"
			ret += currLine
			// and indent
			currLine = keywordLineIndent
		} else {
			currLine += " "
		}
		currLine += pStr
	}
	ret += currLine + "\n"
	return ret
}
func (k *keyword) Name() string {
	return k.name
}
func (k *keyword) Parameters() []Parameter {
	ret := make([]Parameter, len(k.params))
	for n, p := range k.params {
		ret[n] = p
	}
	return ret
}

type parameterBase struct {
	name  string
	value parameterValue
}

func (p *parameterBase) Value() interface{} {
	if p.value == nil {
		return nil
	}
	return p.value.Value()
}
func (p *parameterBase) Name() string {
	return p.name
}
func (p *parameterBase) String() string {
	ret := p.name
	if p.value != nil {
		ret += "=" + p.value.String()
	}
	return ret
}

type parameterValue interface {
	fmt.Stringer
	Value() interface{}
}

type parameterString struct {
	quoted bool
	value  string
}

func (p *parameterString) Value() interface{} {
	return p.value
}
func (p *parameterString) String() string {
	if p.quoted {
		return `"` + strings.ReplaceAll(p.value, `"`, `\\"`) + `"`
	}
	return p.value
}

type parameterInt struct {
	value int
}

func (p *parameterInt) Value() interface{} {
	return p.value
}
func (p *parameterInt) String() string {
	return fmt.Sprintf("%d", p.value)
}

type parameterFloat struct {
	value float64
}

func (p *parameterFloat) Value() interface{} {
	return p.value
}
func (p *parameterFloat) String() string {
	return fmt.Sprintf("%g", p.value)
}

// func Parse(r io.ReadSeeker) (InputFile, error) {
// 	return &inputFile{}, nil
// }

// ErrEolMixed is reported when mixed line endings are found
var ErrEolMixed error = fmt.Errorf("The file contains mixed line endings")

// ErrEolCr is reported when CR line endings are found
var ErrEolCr error = fmt.Errorf("The file contains CR line endings")

type FilePosition struct {
	Line     int64
	Column   int64
	Position int64
}

type ErrSyntax interface {
	Error() string
	Position() FilePosition
}
type errSyntax struct {
	err string
	pos FilePosition
}

func (e *errSyntax) Error() string {
	return e.err
}
func (e *errSyntax) Position() FilePosition {
	return e.pos
}

// RoughParseResults is returned by RoughParse
type roughParseResults struct {
	LineCount    int64
	CommentCount int64
	Keywords     []keyword
	Error        error
}

type FileLike interface {
	io.Closer
	ReadSeekNamer
}

type ReadSeekNamer interface {
	Namer
	io.ReadSeeker
}

func getSeekerSize(s io.Seeker) uint64 {
	start, _ := s.Seek(0, io.SeekCurrent)
	end, _ := s.Seek(0, io.SeekEnd)
	s.Seek(start, io.SeekStart)
	return uint64(end)
}

func NewInputFromFile(f FileLike) InputFileBase {
	return &inputFile{
		Closer: f,
		r:      f,
		Namer:  f,
		size:   getSeekerSize(f),
	}
}
func NewInputFromNamedReader(rs ReadSeekNamer) InputFileBase {
	return &inputFile{
		Closer: nil,
		r:      rs,
		Namer:  rs,
		size:   getSeekerSize(rs),
	}
}

type namedReader struct {
	io.ReadSeeker
	name string
}

func (n *namedReader) Name() string {
	return n.name
}

func NewNamedReader(name string, rs io.ReadSeeker) ReadSeekNamer {
	return &namedReader{
		ReadSeeker: rs,
		name:       name,
	}
}

func lineEndsWithComma(b []byte) bool {
	for i := len(b) - 1; i >= 0; i-- {
		if !(unicode.IsSpace(rune(b[i]))) {
			return b[i] == ','
		}
	}
	return false
}

// RoughParse finds file positions of the start of keywords.
// This is the fastest way to find keywords of the tools provided
// in this package.  If CR or mixed line endings are found, the
// function returns immediately with the appropriate Error in
// RoughParseResults.
func roughParse(r io.ReadSeeker) (ret roughParseResults) {
	s := alleolscanner.NewScannerOptions(
		bufio.NewReader(r),
		alleolscanner.ScannerOptions{
			KeepEndings:     true,
			ExitImmediately: true,
		},
	)
	var t []byte
	var pos int64
	var col int
	var inCommentLine bool
	var inKeywordLine bool
	var lastKeyword *keywordPositionData
	var keywordLine string
	var keywords map[int]keyword = make(map[int]keyword)

	// parse keywords at the same time
	var parsedKeywordWait sync.WaitGroup
	var keywordRawChan chan struct {
		pos  keywordPositionData
		line string
	} = make(chan struct {
		pos  keywordPositionData
		line string
	}, 100)

	var keywordError error

	// goroutine for keyword parsing
	parsedKeywordWait.Add(1)
	go func() {
		defer parsedKeywordWait.Done()
		i := 0
		for c := range keywordRawChan {
			kw, err := parseKeywordLine([]byte(c.line))
			if err != nil {
				epos := err.Position()
				epos.Position += c.pos.start.Position
				epos.Line += c.pos.start.Line - 1
				if epos.Line == 1 {
					epos.Column += c.pos.start.Column - 1
				}
				keywordError = &errSyntax{
					err: fmt.Sprintf("%s: %s", err.Error(), string(c.line)),
					pos: epos,
				}
			} else {
				kw.position = c.pos
				keywords[i] = *kw
			}
			i++
		}
	}()

	for s.Scan() {
		t = s.Bytes()
		ret.LineCount++
		inCommentLine = false

		// column; 1-indexed
		col = 0
		for t[col] == ' ' || t[col] == '\t' {
			col++
		}

		if t[col] == '*' {
			if t[col+1] == '*' {
				inCommentLine = true
				ret.CommentCount++
				if col != 0 {
					ret.Error = &errSyntax{
						err: "Invalid comment",
						pos: FilePosition{
							Column:   int64(col + 1),
							Line:     ret.LineCount,
							Position: pos + int64(col),
						},
					}
					return
				}
			} else {
				// add previous keyword, if there
				if lastKeyword != nil {
					keywordRawChan <- struct {
						pos  keywordPositionData
						line string
					}{
						pos:  *lastKeyword,
						line: keywordLine,
					}
				}
				keywordLine = ""
				// just assume this is the start of the keyword; don't validate yet
				lastKeyword = &keywordPositionData{
					start: FilePosition{
						Column:   int64(col + 1),
						Line:     ret.LineCount,
						Position: pos + int64(col),
					},
				}
				inKeywordLine = true
			}
		}

		if inKeywordLine {
			if !inCommentLine {
				inKeywordLine = lineEndsWithComma(t)
			}
			if len(keywordLine) == 0 {
				keywordLine += string(t[col:])
				lastKeyword.keywordLength += int64(len(t) - col)
			} else {
				keywordLine += string(t)
				lastKeyword.keywordLength += int64(len(t))
			}
		} else if lastKeyword != nil {
			lastKeyword.dataLength += int64(len(t))
			if !inCommentLine {
				lastKeyword.dataLines++
			}
		}

		pos += int64(len(t))
	}
	// append last keyword
	if lastKeyword != nil {
		keywordRawChan <- struct {
			pos  keywordPositionData
			line string
		}{
			pos:  *lastKeyword,
			line: keywordLine,
		}
	}
	close(keywordRawChan)
	parsedKeywordWait.Wait()

	// gather keywords
	ret.Keywords = make([]keyword, len(keywords))
	for k, v := range keywords {
		ret.Keywords[k] = v
	}
	if keywordError != nil {
		ret.Error = keywordError
	} else {
		ret.Error = s.Err()
	}
	return
}
