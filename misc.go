package main

type FilePosition struct {
	Position int64
	Line     int64
	Column   int64
}

func NewFilePosition() FilePosition {
	return FilePosition{0, 1, 1}
}

func (f *FilePosition) AddPosition(other FilePosition) {
	f.Position += other.Position
	f.Line += other.Line - 1
	if f.Line == other.Line {
		f.Column += other.Column - 1
	} else {
		f.Column = other.Column
	}
}

func (f *FilePosition) Add(length int64) {
	f.Position += length
	f.Column += length
}

func (f *FilePosition) AddLine(length int64) {
	f.Position += length
	f.Line++
	f.Column = 1
}
