module gitlab.com/tbhartman/abqt

go 1.13

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/tbhartman/alleolscanner v1.0.1
	gitlab.com/tbhartman/go-matchedfs v1.0.2 // indirect
)
